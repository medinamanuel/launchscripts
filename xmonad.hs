import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import System.IO
import XMonad.Hooks.SetWMName
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.EwmhDesktops
import XMonad.Layout.Dishes
import XMonad.Layout.DragPane
import XMonad.Layout.Grid
import XMonad.Layout.NoBorders
import XMonad.Layout.ThreeColumns
import XMonad.Layout.Circle
import XMonad.Layout.Spiral
import XMonad.Layout.Tabbed
import XMonad.Layout.OneBig
import XMonad.Layout.ThreeColumns
import XMonad.Layout.IM
import XMonad.Layout.Accordion
import XMonad.Layout.PerWorkspace
import XMonad.Layout.LayoutCombinators hiding ( (|||) )
import XMonad.Layout.Reflect
import XMonad.Layout.MultiToggle
import XMonad.Actions.GridSelect
import XMonad.Prompt
import XMonad.Prompt.Window
import XMonad.Prompt.AppendFile
import Data.Ratio ((%))
import Data.List
import Data.Char (toLower)
import qualified XMonad.StackSet as W
import XMonad.Util.Spotify
import qualified DBus as D
import qualified DBus.Client as D
import qualified Codec.Binary.UTF8.String as UTF8


visible = "#c185a7"
bgVisible = "#eeebf1"
hidden = "#5f7962"
active =  "#00a300"
urgent = "#cc0000"


myManageHook = composeAll
	     [ className =? "Emacs24" --> doShift "4"
             , className =? "Gvim" --> doShift "4"
             , className =? "Gedit" --> doShift "4"
             , className =? "Nautilus" --> doShift "8"
	     , className =? "Nemo" --> doShift "8"
	     , className =? "Firefox" --> doShift "2"
             , className =? "jetbrains-pycharm-ce" --> doShift "3"
	     , className =? "evince" --> doShift "6"
             , className =? "libreoffice" --> doShift "apps"
             , className =? "Gnome-calculator" --> doFloat
             , className =? "xterm" --> doShift "1"
             , className =? "URxvt" --> doShift "1"
             , className =? "Google-chrome" --> doShift "2"
             , className =? "MPlayer" --> doFloat <+> doShift "9"
             , className =? "Gimp" --> doShift "6"
             , className =? "Gtk-recordMyDesktop" --> doFloat
             , className =? "Meld" --> doShift "6"
             , className =? "" --> doShift "5"
             , manageDocks
	     ]

myManageHook' = composeOne [ isFullscreen -?> doFullFloat ]

inStr :: String -> String -> Bool
inStr a b = map toLower a `isInfixOf` map toLower b

myGotoConfig = def
 {
    font = "-microsoft-meiryo-medium-*-*-*-9-*-*-*-*-*-*-*"
  , promptBorderWidth = 1        
  , height = 18                     
  , historySize = 256          
  , position = Bottom
  , completionKey = (0, xK_Tab)
  , autoComplete = Nothing                  
  , searchPredicate = inStr                 
  , bgColor = "#133d2f"
  , fgColor = "#f8f8f8"      
 }

myBringConfig =  def
 {
    font = "-microsoft-meiryo-medium-*-*-*-9-*-*-*-*-*-*-*"
  , promptBorderWidth = 1        
  , height = 18                     
  , historySize = 256          
  , position = Bottom
  , completionKey = (0, xK_Tab)
  , autoComplete = Nothing                  
  , searchPredicate = inStr                 
  , bgColor = "#0c1021"
  , fgColor = "#f8f8f8"      
 }
             
toDoList = "/home/mmedina/Documents/TODOList"

main = do

  dbus <- D.connectSession
  D.requestName dbus (D.busName_ "ord.xmonad.Log")
	[D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]

  xmonad $ ewmh $ docks $ def {
               terminal = "urxvt -fg white -bg black -tr -sh 20 -fn 'xft:FuraCode Nerd Font:pixelsize=11' +sb"
             , modMask = mod4Mask
             , startupHook = myStartupHook
	     , manageHook = manageDocks <+> myManageHook <+> myManageHook' <+> manageHook defaultConfig
             , layoutHook = myLayout
             , logHook = dynamicLogWithPP (myLogHook dbus)
             , handleEventHook = handleEventHook defaultConfig <+> fullscreenEventHook
             , focusedBorderColor = "#00A300"
             , workspaces = ["1","2","3","4","5","6","7","8","9"]
             } `additionalKeys`
	     [ ((controlMask, xK_Print), spawn "sleep 0.2; scrot -s")
	     , ((0, xK_Print), spawn "scrot")
	     , ((mod4Mask .|. shiftMask, xK_w), spawn "setwallpaper")
             , ((mod4Mask, xK_Up), spawn "amixer -q -D pulse sset Master 1%+")
             , ((mod4Mask, xK_Down), spawn "amixer -q -D pulse sset Master 1%-")
             , ((mod4Mask .|. shiftMask, xK_l), spawn "xscreensaver-command -lock")
             , ((mod4Mask .|. shiftMask, xK_b), withFocused $ windows . W.sink)
             , ((mod4Mask, xK_b), sendMessage $ ToggleStruts)
             , ((mod4Mask, xK_p), spawn "dmenu_run -b -fn 'Inconsolata-10' -nb '#3333FF' -nf '#FFFF33' -sb '#FF3300' -sf '#FFFF33'")
             , ((mod4Mask .|. shiftMask, xK_x), sendMessage $ Toggle REFLECTX)  
             , ((mod4Mask .|. shiftMask, xK_y), sendMessage $ Toggle REFLECTY)  
             , ((mod4Mask, xK_g), goToSelected defaultGSConfig)
             , ((mod4Mask, xK_m), windowPromptGoto myGotoConfig)
             , ((mod4Mask, xK_n), windowPromptBring myBringConfig)
             , ((mod4Mask, xK_t), do  
                   spawn("date +\"%Y/%m/%d %H:%M:%S\" >>" ++ toDoList)
                   appendFilePrompt myGotoConfig toDoList)
             , ((mod4Mask .|. shiftMask, xK_Right), audioNext)
             , ((mod4Mask .|. shiftMask, xK_Left), audioPrev)
             , ((mod4Mask .|. shiftMask, xK_Down), audioPlayPause)
	     ]

myStartupHook = do
	setWMName "LG3D"
	spawn "/home/mmedina/LaunchScripts/launch_polybar.sh"


myLogHook :: D.Client -> PP
myLogHook dbus = def
	{ ppOutput = dbusOutput dbus
        , ppCurrent = wrap ("%{B" ++ active ++ "} ") " %{B-}"
	, ppVisible = wrap ("%{B" ++ bgVisible ++ "} %{F" ++ active ++ "}") " %{B-} %{F-}"
	, ppUrgent = wrap ("%{F" ++ urgent ++ "} ") " %{F-}"
	, ppHidden = wrap " " " "
	, ppWsSep = ""
	, ppSep = " | "
	, ppTitle = shorten 60
	}

dbusOutput :: D.Client -> String -> IO ()
dbusOutput dbus str = do
	let signal = (D.signal objectPath interfaceName memberName) {
			D.signalBody = [D.toVariant $ UTF8.decodeString str]
		}
        D.emit dbus signal
     where
	objectPath = D.objectPath_ "/org/xmonad/Log"
	interfaceName = D.interfaceName_ "org.xmonad.Log"
	memberName = D.memberName_ "Update"


showLayoutName :: String -> String
showLayoutName a | Just rest <- stripPrefix "combining" a = "Tabbed + DragPane H"
showLayoutName a = a


effectiveLayouts = ThreeColMid 1 (3/100) (1/3) ||| tiled ||| spiral (6/7) ||| simpleTabbed ||| Accordion ||| Dishes 2 (1/6) ||| dragPane Horizontal 0.1 0.5 ||| dragPane Vertical 0.1 0.5 ||| Grid ||| OneBig (3/4) (3/4) ||| noBorders Full
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = Tall nmaster delta ratio
--     tiled   = ResizableTall nmaster delta ratio []
--     leftTall = reflectHoriz $ Tall nmaster delta ratio
     -- The default number of windows in the master pane
     nmaster = 1

     -- Default proportion of screen occupied by master pane
     ratio   = 3/5

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

myLayout = mkToggle (single REFLECTX) $ mkToggle (single REFLECTY) $ avoidStruts $ smartBorders $ effectiveLayouts

