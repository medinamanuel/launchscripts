#!/bin/bash

outputs=""
nMonitors=0
space=""


configureHomeMonitors () {
	outputs="--output HDMI-0 --primary --mode 1920x1080 --output HDMI-1 --mode 1920x1080 --left-of HDMI-0"
	nMonitors=2
	echo "$outputs,$nMonitors"
}


configureLaptopMonitors () {
	while IFS= read -r line; do
		output=`echo $line | cut -d" " -f1`
	
	        position=""
	
		if [ "$output" == "HDMI-2" ]; then
			position="--left-of eDP-1"
		elif [ "$output" == "DP-1" ]; then
			position="--right-of eDP-1"
		fi
	
		if [ "$nMonitors" -ne 0 ]; then
			mode='1920x1080'
			space=" "
		else
			mode=$(echo $line | cut -d" " -f4 | cut -d+ -f1)
		fi

		outputs+="$space--output $output --mode $mode $position"
		nMonitors=$((nMonitors+1))
	done <<< "$(xrandr --query | grep '\bconnected\b')"

	echo "$outputs,$nMonitors"
}

hostname=$(hostname)

if [[ $hostname == "yggdrasil" ]]; then
	# Home Desktop
	output="$(configureHomeMonitors)"
else
	# Work Laptop
	output="$(configureLaptopMonitors)"	
fi

echo "$output,$hostname"

