#!/usr/bin/env bash

# Terminate already running bar instances
killall -q polybar

# wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done

monitorInformation=$(bash /home/mmedina/LaunchScripts/connected_monitors.sh)
xrandrOutput=$(echo $monitorInformation | cut -d, -f1 | sed 's/ $//g')
nMonitors=$(echo $monitorInformation | cut -d, -f2 | tr -d '\r')
hostname=$(echo $monitorInformation | cut -d, -f3)

nMonitors=$((nMonitors + 0))
echo "Connected monitors: $nMonitors monitors"
echo "$xrandrOutput"

if [[ $hostname == "yggdrasil" ]]; then
	polybar --reload home-desktop &
	polybar --reload home-desktop2 &
else

	polybar --reload laptop &

	if [ "$nMonitors" -gt 1 ]; then
		polybar --reload desktop &
	else
		xrandrOutput="$xrandrOutput --output HDMI-2 --off --output DP-1 --off"
	fi
fi

$(xrandr $xrandrOutput)


#if ! pgrep -x "nm-applet" >/dev/null
#then
#  nm-applet &
#fi
